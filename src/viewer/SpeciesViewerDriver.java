// 
// Author - Jack Hebert (jhebert@cs.washington.edu) 
// Copyright 2007 
// Distributed under GPLv3 
// 
// Modified - Dino Konstantopoulos
// Distributed under the "If it works, remolded by Dino Konstantopoulos, 
// otherwise no idea who did! And by the way, you're free to do whatever 
// you want to with it" dinolicense
// 
package viewer;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.*;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;

import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;

public class SpeciesViewerDriver {

	public static void main(String[] args) {
		int i = 1;
		JobClient client = new JobClient();
		JobConf conf = new JobConf(SpeciesViewerDriver.class);
		// FileInputFormat.setInputPaths(conf, new Path("output"+i));
		while (i < 51) 
		{

			conf.setJobName("Species Viewer");

			// ~dk
			// conf.setInputFormat(org.apache.hadoop.mapred.SequenceFileInputFormat.class);

			conf.setOutputKeyClass(FloatWritable.class);
			conf.setOutputValueClass(Text.class);

			if (args.length < 2) {
				System.out.println("Usage: SpeciesViewerDriver <input path> <output path>");
				System.exit(0);
			}

			// ~dk
			// conf.setInputPath(new Path(args[0]));
			// conf.setOutputPath(new Path(args[1]));
			// FileInputFormat.setInputPaths(conf, new Path(args[0]));
			// FileOutputFormat.setOutputPath(conf, new Path(args[1]));
			int nextValue = i + 1;
			// FileInputFormat.setInputPaths(conf, new Path(args[0]));
			// FileOutputFormat.setOutputPath(conf, new Path(args[1]));
			FileInputFormat.setInputPaths(conf,
					new Path("output" + i));
			// FileInputFormat.setInputPaths(conf, new Path("output" + i));
			FileOutputFormat.setOutputPath(conf, new Path("outputs" + i));
			//FileInputFormat.setInputPaths(conf, "/home/hduser/Documents/outputt" + i);
			//int newFileVal = i+1;
			//FileOutputFormat.setOutputPath(conf, new Path("/home/hduser/Documents/outputtt" + newFileVal));

			conf.setMapperClass(SpeciesViewerMapper.class);
			conf.setReducerClass(org.apache.hadoop.mapred.lib.IdentityReducer.class);

			client.setConf(conf);
			try {
				JobClient.runJob(conf);
			} catch (Exception e) {
				e.printStackTrace();
			}
			i++;
			
		}
	}
}
