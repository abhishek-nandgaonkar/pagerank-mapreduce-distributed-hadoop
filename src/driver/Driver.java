package driver;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

import java.util.ArrayList;

import org.apache.hadoop.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.SequenceFileInputFormat;

import builder.SpeciesGraphBuilder;
import builder.SpeciesGraphBuilderMapper;
import builder.SpeciesGraphBuilderReducer;
import builder.XmlInputFormat;
import iterator.SpeciesIterDriver2;
import iterator.SpeciesIterMapper2;
import iterator.SpeciesIterReducer2;
import viewer.SpeciesViewerDriver;
import viewer.SpeciesViewerMapper;

//import org.apache.nutch.parse.Parse; 
//import org.apache.nutch.parse.ParseException; 
//import org.apache.nutch.parse.ParseUtil; 
//import org.apache.nutch.protocol.Content; 

import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;

public class Driver {
	public static ArrayList<String> outlinks = null;

	public static void main(String[] args) throws Exception {
		JobClient client1, client2 = null;
		JobConf conf1 = null, conf2 = null;
		// SpeciesGraphBuilderMapper.outlinks = new ArrayList();
		JobClient client = new JobClient();
		outlinks = new ArrayList();
		JobConf conf = new JobConf(SpeciesGraphBuilder.class);
		conf.setJobName("Page-rank Species Graph Builder");

		conf.setMapperClass(SpeciesGraphBuilderMapper.class);
		conf.setMapOutputKeyClass(Text.class);
		conf.setMapOutputValueClass(Text.class);

		conf.set("start", "<page>");
		conf.set("end", "</page>");

		// conf.setInputFormat(org.apache.hadoop.mapred.TextInputFormat.class);
		// conf.setOutputFormat(org.apache.hadoop.mapred.SequenceFileOutputFormat.class);
		conf.setInputFormat(XmlInputFormat.class);
		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(Text.class);

		conf.setReducerClass(SpeciesGraphBuilderReducer.class);
		// conf.setCombinerClass(SpeciesGraphBuilderReducer.class);

		// conf.setInputPath(new Path("graph1"));
		// conf.setOutputPath(new Path("graph2"));
		// take the input and output from the command line
		FileInputFormat.setInputPaths(conf, new Path(args[0]));
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));

		client.setConf(conf);

		try {
			JobClient.runJob(conf);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		Thread t1 = new Thread(new Runnable() {
		     public void run() {
		    	 String[] arguments = new String[] {"output0", "output1"};
		    	 SpeciesIterDriver2.main(arguments);
		     }
		});  
		t1.start();t1.join();
		Thread.sleep(10000);
		
		Thread t2 = new Thread(new Runnable() {
		     public void run() {
		    	 try {
					t1.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    	 String[] arguments = new String[] {"output1", "output1"};
		    	 SpeciesViewerDriver.main(arguments);
		     }
		});  
		t2.start();
		// ***********************************************************************

//		S

		// ********************************************************************************************

//		int i2 = 1;
//		client2 = new JobClient();
//		conf2 = new JobConf(SpeciesViewerDriver.class);
//		// FileInputFormat.setInputPaths(conf, new Path("output"+i));
//		while (i2 < 51) {
//
//			conf.setJobName("Species Viewer");
//
//			// ~dk
//			// conf.setInputFormat(org.apache.hadoop.mapred.SequenceFileInputFormat.class);
//
//			conf.setOutputKeyClass(FloatWritable.class);
//			conf.setOutputValueClass(Text.class);
//
//			if (args.length < 2) {
//				System.out.println("Usage: SpeciesViewerDriver <input path> <output path>");
//				System.exit(0);
//			}
//
//			// ~dk
//			// conf.setInputPath(new Path(args[0]));
//			// conf.setOutputPath(new Path(args[1]));
//			// FileInputFormat.setInputPaths(conf, new Path(args[0]));
//			// FileOutputFormat.setOutputPath(conf, new Path(args[1]));
//			int nextValue = i2 + 1;
//			// FileInputFormat.setInputPaths(conf, new Path(args[0]));
//			// FileOutputFormat.setOutputPath(conf, new Path(args[1]));
//			FileInputFormat.setInputPaths(conf,
//					new Path("/home/abhishek/workspace/Assignment3_Iterator/" + "output" + i));
//			// FileInputFormat.setInputPaths(conf, new Path("output" + i));
//			FileOutputFormat.setOutputPath(conf, new Path("outputs" + i));
//			// FileInputFormat.setInputPaths(conf,
//			// "/home/hduser/Documents/outputt" + i);
//			// int newFileVal = i+1;
//			// FileOutputFormat.setOutputPath(conf, new
//			// Path("/home/hduser/Documents/outputtt" + newFileVal));
//
//			conf.setMapperClass(SpeciesViewerMapper.class);
//			conf.setReducerClass(org.apache.hadoop.mapred.lib.IdentityReducer.class);
//
//			client.setConf(conf);
//			try {
//				JobClient.runJob(conf);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			i2++;
//
//		}
//		try {
//			JobClient.runJob(conf2);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
	}
}